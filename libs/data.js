let Mock = require('mockjs');

let area_arr = require('../pages/location/area.json');
let pi = Math.ceil(Math.random()*23);
let province = area_arr[pi]
let citys = province.childs.map((item) => {
  return item.name
})
// let ci = Math.ceil(Math.random()*citys.length); 
// let city = province.childs[ci]
let num = Mock.mock({
  "obj|100": [{
    "searchValue": null,
    "createBy": null,
    "createTime": "@datetime",
    "updateBy": null,
    "updateTime": "@datetime",
    "remark": null,
    "params": {},
    "id": "@increment",
    "type": 0,
    "houseNum|1": ["一室","两室","三室"],
    "houseHall|1": ["一厅","两厅","三厅"],
    "toiletNum|1": ["一卫","两卫","三卫"],
    "houseCode":  function() {return this.houseNum+this.houseHall+this.toiletNum},
    "houseArea|60-200": 100,
    "roomType": null,
    "roomArea": null,
    "direction": null,
    "decoration|1": ["精装","简装"],
    "price": /[1-9]\d00/,
    "startDate": "2022-12-21",
    "state": 1,
    "floor": null,
    "stepType": null,
    "introduce": function() {return this.houseCode+"可做宿舍，包物业费\n钥匙在门口，民用水电"},
    "villageId": 12,
    "villageName|1": ["盛世豪庭","嘉林景苑","锦绣雷庭","紫金苑","中央公馆","云海山庄","洲际观天下"],
    "villageCity|1": citys,
    "address":  function() {return province.name+this.villageCity+"东吴路与通运口交叉口"},
    "roomCode": null,
    "houseNo": /[1-9]\d{2,3}/,
    "code": "2046333880",
    "payType": "月付",
    "publishId": 280,
    "agentName": "@cname",
    "agentPhone": /^1[3456789]\d{9}$/,
    "agentAvatar": "",
    "agentUserId": 280,
    "ownerName": function() {return this.agentName},
    "owerPhone": function() {return this.agentPhone},
    "createName": "@datetime",
    "updateName": "@datetime",
    "faceUrl|1": ["/profile/upload/2023/03/21/83d8ae46-3b9e-4605-bd4a-b81b70e0e962.jpg",
    "/profile/upload/2022/12/21/514dda76-6acf-43cc-9d93-e8ef0dc631ae.jpg",
    "/upload/2022/12/21/f0ca48d7-ca6b-48ee-a9f6-5fc5a39dfb69.jpg",
  "/profile/upload/2022/12/17/4ab0ad93-6166-4bbc-a715-ac59b0f736ab.jpg",
  "/profile/upload/2022/12/16/8e99c268-d170-4fe9-aead-602de13768b6.jpg",
  "/profile/upload/2022/12/16/fe37a124-b725-4f97-bb89-e128b50e6516.jpg",],
    "imageList|3-6": ["/profile/upload/2023/03/21/83d8ae46-3b9e-4605-bd4a-b81b70e0e962.jpg",
    "/profile/upload/2022/12/21/514dda76-6acf-43cc-9d93-e8ef0dc631ae.jpg",
    "/upload/2022/12/21/f0ca48d7-ca6b-48ee-a9f6-5fc5a39dfb69.jpg",
  "/profile/upload/2022/12/17/4ab0ad93-6166-4bbc-a715-ac59b0f736ab.jpg",
  "/profile/upload/2022/12/16/8e99c268-d170-4fe9-aead-602de13768b6.jpg",
  "/profile/upload/2022/12/16/fe37a124-b725-4f97-bb89-e128b50e6516.jpg",],
    "featureList": null,
    "village": null,
    "feature": null,
    "heart": false,
    "longitude": "120.12197",
    "latitude": "30.3405"
  }]
})
console.log(num)
exports.houseList = num