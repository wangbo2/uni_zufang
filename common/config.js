// 全局配置文件
// let baseUrl = 'http://42.192.207.140:8083';
// let staticUrl = 'http://42.192.207.140:8083';
let baseUrl = 'https://sourcebyte.vip';
let staticUrl = 'https://sourcebyte.vip';
// let baseUrl = 'https://sourcebyte.cn';
// let staticUrl = 'https://sourcebyte.cn';
// let baseUrl = 'https://gelin8.com';
// let staticUrl = 'https://gelin8.com';
// 版本号 用于更新
let version = 1;
// vuex_version版本号 用于显示
let vuex_version = '1.0.' + version;
// 是否需要热更新（后台自动更新）
let flag_hot = false;

export default {
	baseUrl: baseUrl,
	staticUrl:staticUrl,
	version: version,
	vuex_version: vuex_version,
}

